import PriviteLogin from './JavascriptMiddleHandle/PriviteLogin';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import App from './App';
import Booking from './Component/Booking';
import Header from './Component/Header';
import Information from './Component/Information';
import store from './MiddleWare/store';
import './Sass/index.scss'
import Login from './Component/Login';
import KeepStateInContext from './ContextLocation/middleKeepStateBooking';
import KeepSessionInContext from './ContextLocation/middleKeepSesion';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <KeepSessionInContext>
      <KeepStateInContext>
        <Provider store={store}>
          {/* navigater */}
          <Header />
          <Routes>
            <Route index element={<App />} />
            <Route path='/Login' element={<Login />} />
            <Route path=':NowCommingSoon' element={<App />} />
            <Route path=':NowCommingSoon/:Slug' element={<Information />} />
            <Route path=':NowCommingSoon/:Slug/BooKing'
              element={<PriviteLogin>
                <Booking />
              </PriviteLogin>} />
          </Routes>
        </Provider>
      </KeepStateInContext>
    </KeepSessionInContext>
  </BrowserRouter>
)
