import { useContext, useState } from "react";
import { createContext } from "react";

const ContextSession = createContext()
export const useContextSession = () => {
    return useContext(ContextSession)
}

export default function KeepSessionInContext({ children }) {
    const [session, setSession] = useState({})
    return (<ContextSession.Provider value={{ session, setSession }}>
        {children}
    </ContextSession.Provider>)
}