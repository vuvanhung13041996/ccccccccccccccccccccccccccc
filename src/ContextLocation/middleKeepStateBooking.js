import { useContext, useState } from "react";
import { createContext } from "react";

const ContextBooking = createContext()
export const useContextBooking = () => {
    return useContext(ContextBooking)
}

export default function KeepStateInContext({ children }) {
    const [state, setState] = useState({})
    return (<ContextBooking.Provider value={{state, setState}}>
        {children}
    </ContextBooking.Provider>)
}