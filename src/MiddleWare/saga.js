import { call, put, takeLatest } from 'redux-saga/effects'
import { actions, constants } from '.'
// handle movie now and soon
async function handleAPINowAndSoon() {
    let reponse = await fetch('https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/nowAndSoon')
    let nowandsoonobject = await reponse.json()
    return nowandsoonobject
}

function* midleCallNowAndSon() {
    let nowandsoonobject = yield call(handleAPINowAndSoon)
    yield put(actions.PUSHAPINOWANDSOON(nowandsoonobject))
}

// handle cine movie show in curent time
async function handleAPICineShowMovie(idmovie) {
    let reponsecine = await fetch(`https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/movie/${idmovie}`)
    let listcineshowmovie = await reponsecine.json()
    // listcity
    let reponselistcity = await fetch(`https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/city`)
    let listcity = await reponselistcity.json()
    return {
        listcineshowmovie,
        listcity
    }
}
function* midleCallCineShowMovie({ payload }) {
    let objectcineandcity = yield call(handleAPICineShowMovie, payload)
    yield put(actions.PUSHAPILISTCINESHOWMOVIE(objectcineandcity))
}


// BOOKING
async function handleAPIBooking() {
    let responseBooking = await fetch(`https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/booking/detail`)
    let booking = await responseBooking.json()
    return booking
}
function* midleCallBooking() {
    let booking = yield call(handleAPIBooking)
    yield put(actions.PUSHAPIBOOKING(booking))
}

// saga
function* midleSagaAPI() {
    yield takeLatest(constants.CALL_API_NOW_AND_SOON, midleCallNowAndSon);
    yield takeLatest(constants.CALL_API_LIST_CINE_SHOW_MOVIE, midleCallCineShowMovie);
    yield takeLatest(constants.CALL_BOOKING, midleCallBooking);

}

export default midleSagaAPI