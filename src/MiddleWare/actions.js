import { constants } from ".";


// HANDLE ACTIONS API NOW AND SOON
export const CALLAPINOWANDSOON = (payload) => {
    return {
        type: constants.CALL_API_NOW_AND_SOON,
        payload
    }
}
export const PUSHAPINOWANDSOON = (payload) => {
    return {
        type: constants.PUSH_API_NOW_AND_SOON,
        payload
    }
}


// HANDLE ACTIONS API LIST CINEMAS SHOW MOVIE
export const CALLAPILISTCINESHOWMOVIE = (payload) => {
    return {
        type: constants.CALL_API_LIST_CINE_SHOW_MOVIE,
        payload
    }
}
export const PUSHAPILISTCINESHOWMOVIE = (payload) => {
    return {
        type: constants.PUSH_API_LIST_CINE_SHOW_MOVIE,
        payload
    }
}


// HANDLE ACTIONS API LIST CINEMAS SHOW MOVIE
export const CALLAPIBOOKING = (payload) => {
    return {
        type: constants.CALL_BOOKING,
        payload
    }
}
export const PUSHAPIBOOKING = (payload) => {
    return {
        type: constants.PUSH_BOOKING,
        payload
    }
}