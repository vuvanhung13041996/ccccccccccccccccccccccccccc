import { constants } from ".";


let initialvalue = {
    movieCommingSoon: [],
    movieShowing: [],
    listcineshowmovie: undefined,
    listcity: undefined,
    Booking: {
        ticket: [],
        consession: [],
        seatPlan: {}
    }
}

const reducer = (state = initialvalue, { type, payload }) => {
    let newstate
    switch (type) {
        // NOW SOON
        case constants.PUSH_API_NOW_AND_SOON:
            newstate = {
                ...state,
                ...payload
            }
            break;
        // CINE SHOW MOVIE
        case constants.PUSH_API_LIST_CINE_SHOW_MOVIE:
            newstate = {
                ...state,
                ...payload
            }
            break;
        // BOOKING
        case constants.PUSH_BOOKING:
            newstate = {
                ...state,
                Booking: { ...payload }
            }
            break;
        default:
            newstate = {
                ...state
            }
            break;
    }
    return newstate
}

export default reducer