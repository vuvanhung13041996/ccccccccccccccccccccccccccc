import { applyMiddleware, combineReducers, createStore } from "redux";
import reduxSaga from "redux-saga";
import reducer from "./reducer";
import midleSagaAPI from "./saga";


const midleSaga = reduxSaga()

const globalstate = combineReducers({
    reducer
})


const store = createStore(
    globalstate,
    applyMiddleware(midleSaga)
)

midleSaga.run(midleSagaAPI)
export default store