import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useContextSession } from '../ContextLocation/middleKeepSesion';
import { useContextBooking } from '../ContextLocation/middleKeepStateBooking';
import middleHandleSeatHasBook from '../JavascriptMiddleHandle/middleHandleSeatHasBook';
import midleRenderSeat from '../JavascriptMiddleHandle/midleRenderSeat';
import '../Sass/Booking.scss'

const Booking = ({ Booking }) => {
  const { state } = useContextBooking()
  const { session } = useContextSession()
  console.log('state', state);
  console.log('state', session);

  // seat has book
  const [tickethasbeen, setTicketHasBeen] = useState([])
  // seat in cine
  const [seat, setSeat] = useState([])
  // render seat
  useEffect(() => {
    if (Booking?.seatPlan?.seatLayoutData?.areas) {
      setSeat(midleRenderSeat(Booking))
    }
  }, [Booking])
  // seat dã book
  useEffect(() => {
    fetch(`https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/Ticket`)
      .then(res => res.json())
      .then(data => {
        setTicketHasBeen(middleHandleSeatHasBook(data, session))
      })
  }, [])

  // const handleBookingSeat = (event) => {

  //   let value = event.target.innerText
  //   console.log(value);
  //   if (!event.target.setbackground) {
  //     event.target.style.backgroundColor = "red"
  //     event.target.setbackground = true
  //     setListcheir(prev => [...prev, value])
  //   }
  //   else {
  //     event.target.style.backgroundColor = "burlywood"
  //     event.target.setbackground = undefined
  //     setListcheir(prev => [...prev].filter(element => element !== value))
  //   }
  // }
  // let listSpan = document.getElementsByClassName("seat-event");
  // for (let index = 0; index < listSpan.length; index++) {
  //   listSpan[index].addEventListener('click', (event) => {
  //     event.preventDefault()
  //     handleBookingSeat(event)
  //   })
  // }


  return (<>
    <div className='container-seat'>{seat}</div>
  </>)
}

const MapStateToProps = ({ reducer }) => {
  let Booking = reducer.Booking
  return {
    Booking
  }
}

export default connect(MapStateToProps, undefined)(memo(Booking))
