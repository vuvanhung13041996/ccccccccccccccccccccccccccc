import React, { memo, useEffect, useState } from 'react'
import { useParams } from 'react-router'
import '../Sass/Information.scss'
import parse from 'html-react-parser';
import Filter from './Filter';
import { URL, TOKEN } from '../utils/constant';
import { useContextBooking } from '../ContextLocation/middleKeepStateBooking';



function Information() {
    // get slug
    const { Slug } = useParams()
    // setslung
    const [information, setInformation] = useState({})
    const { setState } = useContextBooking()

    // state
    useEffect(() => {
        fetch(`${URL}/${TOKEN}/cinema/movieBySlug/${Slug}`)
            .then(inforresponse => inforresponse.json())
            .then(information => {
                setInformation(information)
                setState({
                    imageLandscape: information.imageLandscape,
                    imagePortrait: information.imagePortrait,
                    FilmName: information.subName || information.name,
                })
            })
    }, [])
    return (<>
        <div className='information-container'>
            <div className="information-card">
                <div className="information-card-littleinfor">
                    <img src={information.imagePortrait} alt="MOVIE" />
                    <h1>{information.subName || information.name}</h1>
                </div>
                <div>{parse(information.description || '')}</div>
            </div>
            <Filter />
        </div>
    </>)
}

export default memo(Information)