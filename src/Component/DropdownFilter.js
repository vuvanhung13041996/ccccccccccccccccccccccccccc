const DropdownFilter = ({
  handleChange,
  defaultText,
  defaultTextWhenNoItem,
  listItems
}) => {

  if (!listItems?.length) {
    return (
      <select>
        <option>{defaultTextWhenNoItem}</option>
      </select>
    )
  }

  return (
    <select onChange={(event) => handleChange(event)}>
      <option hidden  >{defaultText}</option>
      {
        listItems?.map((element, index) => {
          return <option key={index} value={element.id} >{element.name}</option>
        })
      }
    </select>
  )
}

export default DropdownFilter;