import React, { useState } from 'react'
import { useNavigate } from 'react-router'
import '../Sass/Login.scss'
import { URL, TOKEN } from '../utils/constant'


function Login() {
    const nav = useNavigate()
    const [login, setState] = useState({})
    //  "Email": "vuvanhung13041996@gmail.com",
    //  "Password": "113"
    const handleOnclickLogin = () => {
        fetch(`${URL}/${TOKEN}/user/Login`,
            {
                method: "POST",
                headers: {
                    'accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(login),
            }).then(response => {
                console.log(response);
                if (response.ok) {
                    localStorage.setItem('User', JSON.stringify(login))
                    nav(-1)
                }
            })
    }
    console.log(login);
    const handleChangeValueInput = (event) => {
        let name = event.target.name
        let value = event.target.value

        setState(previus => {
            return {
                ...previus,
                [name]: value
            }
        })
    }
    return (<div id="id01" className="modal">
        <div className="modal-content animate" action="/action_page.php" method="post">
            <div className="container">
                <label htmlFor="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="Email" required onChange={(event) => { handleChangeValueInput(event) }} />

                <label htmlFor="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="Password" required onChange={(event) => { handleChangeValueInput(event) }} />

                <button onClick={() => handleOnclickLogin()}>Đăng Nhập</button>
            </div>

            <div className="container" style={{ backgroundColor: '#f1f1f1' }}>
                <button>Đăng Ký</button>
            </div>
        </div>
    </div>)
}


export default Login