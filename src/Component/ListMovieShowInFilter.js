import React, { memo } from 'react'
import { connect } from 'react-redux';
import { useNavigate } from 'react-router'
import { useContextSession } from '../ContextLocation/middleKeepSesion';
import { actions } from '../MiddleWare';
import '../Sass/Listmovieshowinfilter.scss'

function ListMovieShowInFilter({ listcineshowmovie, idcity, date, idcine, callBooking }) {
    const { setSession } = useContextSession()
    const nav = useNavigate()
    // value is cine passed filter
    let value = listcineshowmovie?.filter(filtercity => {
        // filter city
        return filtercity.cityId.includes(idcity)
    }).filter(filterdate => {
        // filter date if includes date => cine === true
        return filterdate.dates.some(datefil => {
            return datefil.showDate.includes(date)
        })
    }).filter(filtercine => {
        // filter cine flow cine ID
        return filtercine.id.includes(idcine)
    })

    // get session
    const handleClickTime = (session, cineName) => {
        nav('BooKing')
        callBooking()
        setSession({
            ...session,
            cineName: cineName
        });
    }
    return (<div className='Listmovieshowinfilter-container'>
        {/* loop through */}
        {value?.map((cine, index) => {
            // today is date after filter
            let today = cine.dates.filter((datefil) => {
                return datefil?.showDate.includes(date)
            })
            return <div key={index} className='filter-cine'>
                {/* name cine */}
                <h3 className="filter-cine-name">{cine.name}</h3>
                <div className="filter-cine-name-date-time">
                    {/* if no choose date, will show firt date, if choose show date choose */}
                    <div className="filter-cine-date">Ngày Chiếu: {(date.length > 1) ? date : today[0]?.showDate}</div>
                    {/* if no choose date filter will show firt element of dates (today) */}
                    <div className="filter-cine-container-time">{(date.length > 1)
                        // if choose date
                        ? today[0]?.bundles?.map((bundle) => {
                            return (bundle.sessions.map((session, i) => {
                                return <span className="filter-cine-container-time-element" key={i} onClick={() => handleClickTime(session.cine.name)}>{session.showTime}</span>
                            }))
                        })
                        // if no choose date 
                        : today[0]?.bundles?.map((bundle) => {
                            return (bundle.sessions.map((session, i) => {
                                return <span className="filter-cine-container-time-element" key={i} onClick={() => handleClickTime(session, cine.name)}>{session.showTime}</span>
                            }))
                        })}</div>
                </div>
            </div>
        })}
    </div>)
}

const MapDispatchToProps = (dispatch) => {
    return {
        callBooking() {
            dispatch(actions.CALLAPIBOOKING())
        }
    }
}
export default connect(undefined, MapDispatchToProps)(memo(ListMovieShowInFilter))