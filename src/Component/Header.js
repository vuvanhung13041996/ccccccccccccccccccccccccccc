import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Link, useLocation, useParams } from 'react-router-dom'
import { actions } from '../MiddleWare'
import '../Sass/Header.scss'

function Header({ callAPINowAndSoon }) {
    const location = useLocation()
    // active state
    const [active, setActive] = useState(location.pathname)

    useEffect(() => {
        setActive(location.pathname)
    }, [location.pathname])

    useEffect(() => {
        callAPINowAndSoon()
    }, [location.pathname])
    // active handle
    const handleActive = (link) => {
        setActive(link)
    }
    return (
        <div className="topnav">
            <Link onClick={() => handleActive('/Now')} className={active === '/Now' ? "active" : ""} to={'Now'}>Phim Đang Chiếu</Link>
            <Link onClick={() => handleActive('/CommingSoon')} className={active === '/CommingSoon' ? "active" : ""} to={'CommingSoon'}>Phim Sắp Chiếu</Link>
            <Link onClick={() => handleActive('/Login')} className={active === '/Login' ? "active" : ""} to={'Login'}>Login</Link>
        </div>
    )
}


const MapsDistPatchToProps = (dispatch) => {
    return {
        callAPINowAndSoon() {
            dispatch(actions.CALLAPINOWANDSOON())
        }
    }
}
export default connect(undefined, MapsDistPatchToProps)(Header)