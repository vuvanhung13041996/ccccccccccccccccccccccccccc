import React, { memo, useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { actions } from '../MiddleWare';
import '../Sass/Filter.scss'
import ListMovieShowInFilter from './ListMovieShowInFilter';
import DropdownFilter from './DropdownFilter';


const Filter = memo(({ listcineshowmovie: listCineShowMovie, listcity }) => {

  const [idcity, setIdcity] = useState('');
  const [idcine, setIdcine] = useState('');
  const [date, setDate] = useState('');
  const [listcine, setListcine] = useState('');

  // handle filter list city
  useEffect(() => {
    const listCineShow = listCineShowMovie?.filter(cine => {
      return cine.cityId.includes(idcity)
    });

    setListcine(listCineShow)
  }, [idcity]);

  const handleChangeCityID = (event) => {
    setIdcity(event.target.value);
  }

  const handleChangeCineID = (event) => {
    setIdcine(event.target.value);
  }

  const handleChangeDate = (event) => {
    const dateFormated = event.target.value.split('-').reverse().join('/');
    setDate(dateFormated);
  }

  return (
    <div>
      <div className="custom-select">
        <DropdownFilter
          handleChange={handleChangeCityID}
          defaultText='Thành Phố'
          listItems={listcity}
        />
        <input type={'date'} onChange={(event) => { handleChangeDate(event) }}></input>
        <DropdownFilter
          handleChange={handleChangeCineID}
          defaultText='Rạp Đang Chiếu Phim'
          defaultTextWhenNoItem='Không Có Rạp Đang Chiếu Phim'
          listItems={listcine}
        />
      </div>
      <ListMovieShowInFilter listcineshowmovie={listCineShowMovie} idcity={idcity} date={date} idcine={idcine} />
    </div>
  )
});


const MapStateToProps = (globalstate) => {
  let newstate = globalstate.reducer
  return {
    ...newstate
  }
}

const MapsDistPatchToProps = (dispatch) => {
  return {
    // list cine show movie current 
    callAPICineShowMovies(payload) {
      dispatch(actions.CALLAPILISTCINESHOWMOVIE(payload))
    }
  }
}
export default connect(MapStateToProps, MapsDistPatchToProps)(Filter)