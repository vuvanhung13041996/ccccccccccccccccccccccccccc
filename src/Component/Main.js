import React, { memo, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useNavigate, useParams } from 'react-router'
import { actions } from '../MiddleWare'
import '../Sass/Main.scss'


function Main({ movieCommingSoon, movieShowing, callAPICineShowMovies }) {
    const navlink = useNavigate()
    // handle change value render
    const [render, setRender] = useState(movieShowing)
    // get params to setRender value
    const { NowCommingSoon } = useParams()

    useEffect(() => {
        switch (NowCommingSoon) {
            case 'CommingSoon':
                setRender(movieCommingSoon)
                break;
            default:
                setRender(movieShowing)
                break
        }
    }, [NowCommingSoon, movieShowing])
    // handle api infor movie film
    const handleInformationFilm = (slug, id) => {
        NowCommingSoon ? navlink(slug) : navlink('Now' + '/' + slug)
        callAPICineShowMovies(id);
    }
    return (<div className="containernowandsoon">
        {
            render?.map((element, index) => {
                return (
                    <div key={index} className="containernowandsoon-element">
                        <div className="card">
                            <img src={element.imagePortrait} alt="MOVIE" style={{ width: '100%' }} />
                            <h1>{element.subName || element.name}</h1>
                            {/* get slug movie */}
                            <p><button onClick={() => handleInformationFilm(element.slug, element.id)}>Xem Phim</button></p>
                        </div>
                    </div>
                )
            })
        }
    </div>)
}
const MapStateToProps = (globalstate) => {
    let newstate = globalstate.reducer
    return {
        ...newstate
    }
}

const MapsDistPatchToProps = (dispatch) => {
    return {
        // list cine show movie current 
        callAPICineShowMovies(payload) {
            dispatch(actions.CALLAPILISTCINESHOWMOVIE(payload))
        }
    }
}
export default connect(MapStateToProps, MapsDistPatchToProps)(memo((Main)))

