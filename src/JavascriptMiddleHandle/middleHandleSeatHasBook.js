export default function middleHandleSeatHasBook(data, session) {
    const seathasbookobj = data?.filter((element) => {
        return element.ShowCode === session.id
    })
    // list has book
    const seathasbook = []
    for (let i = 0; i < seathasbookobj.length; i++) {
        const element = seathasbookobj[i];
        seathasbook.push(element.SeatCode)
    }


    return (seathasbook)
}
