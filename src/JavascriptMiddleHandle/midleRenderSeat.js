const midleRenderSeat = (Booking) => {
    let rederSeat = Booking?.seatPlan?.seatLayoutData?.areas
    let [typesimple, typecouple] = rederSeat
    // loop seat simple
    const arrSeatSimple = []
    for (let typesimpleindex = 0; typesimpleindex < typesimple.rows.length; typesimpleindex++) {
        const rowSeatSimple = []
        const alphaSeatSimple = typesimple.rows[typesimpleindex].physicalName

        const SeatSimple = typesimple.rows[typesimpleindex].seats
        // console.log(typeSeatSimple);
        for (let indexSeat = 0; indexSeat < SeatSimple.length; indexSeat++) {
            const idSeatSimple = SeatSimple[indexSeat].id
            rowSeatSimple.push(<span key={indexSeat} style={{ border: '1px solid black' }} className='seat-simple seat-event' setbackground='false'>{alphaSeatSimple + idSeatSimple}</span>)
        }
        if (rowSeatSimple.length > 1) {
            arrSeatSimple.push(<div key={typesimpleindex} className='row-simple'>{rowSeatSimple}</div>)
        } else {
            arrSeatSimple.push(<div key={typesimpleindex} className='row-null'>L o i d i</div>)
        }
    }
    // loop couple
    const arrSeatCouple = []
    for (let typecoupleindex = 0; typecoupleindex < typecouple.rows.length; typecoupleindex++) {
        const rowSeatCouple = []
        const alphaSeatCouple = typecouple.rows[typecoupleindex].physicalName
        const SeatCouple = typecouple.rows[typecoupleindex].seats
        for (let indexSeat = 0; indexSeat < SeatCouple.length; indexSeat += 2) {
            const idSeatCouple = SeatCouple[indexSeat].id;
            rowSeatCouple.push(<span key={indexSeat} style={{ border: '1px solid black' }} className='seat-couple seat-event' setbackground='false'>{(alphaSeatCouple + idSeatCouple + '' + alphaSeatCouple + (Number(idSeatCouple) + 1))}</span>)
        }
        arrSeatCouple.push(<div key={alphaSeatCouple} className='row-couple'>{rowSeatCouple}</div>)
    }


    const result = arrSeatSimple.reverse().concat(arrSeatCouple)
    return result
}
export default midleRenderSeat