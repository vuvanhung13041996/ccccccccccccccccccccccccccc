import React, { memo } from 'react'
import { connect } from 'react-redux';
import { Navigate } from 'react-router-dom'

function PriviteLogin({ children }) {
    return (
        <div>{
            localStorage.getItem('User')
                ? children
                : <Navigate to={'/Login'} />
        }</div>
    )
}


export default PriviteLogin